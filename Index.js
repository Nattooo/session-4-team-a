const fs = require('fs')
const path = require('path')
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

const triangle = require('./triangle');
const circle = require('./circle');
const cube = require('./cube');
const square = require('./square');


console.clear();
console.log("Which one you Choose? ");
console.log(`
1. Circle,
2. Square
3. Cube
4. Triangle`)  


rl.question('Answer: ', answer => {
    switch(+answer) {
        case 1:
            console.clear();
            rl.question(`Which One You Want To Calculate? 
            1. Area Of Circle
            2. Round Of Circle 
            Answer: `, answer => {
                if(+answer == 1) {
                    rl.question("Minta Jari-Jari berapa? ", r => {
                        console.log('Ya sudah ini Hasilnya = ', circle.areaOfCircle(r));
                        register(circle.areaOfCircle(r))
                        rl.close();
                    })
                } else if(answer == 2) {
                    rl.question("Minta Jari-Jari bereapa? ", r => {
                        console.log('Ya sudah ini Hasilnya = ', circle.roundOfCircle(r));
                        rl.close();
                    })
                } else {
                    console.log(`Kok Sek Ndablek ${answer} iku Tidak Ada, Blok`)
                    rl.close();
                }
            })
            break
        case 2:
            console.clear();
            rl.question(`Which One You Want To Calculate?
            1. Area Of Square
            2. Round Of Square
            Answer: `, answer => {
                if(+answer == 1) {
                    rl.question('Minta Sisi Berapa ? ', s => {
                        console.log(square.squareArea(+s))
                        rl.close();
                    })
                } else if(+answer == 2) {
                    rl.question('Minta Sisi Berapa ? ', s => {
                        console.log("Ya Sudah Ini Hasilnya: ", square.squareRound(+s))
                        rl.close();
                    })
                } else {
                    console.log(`Kok Sek Ndablek ${answer} iku Tidak Ada`)
                    rl.close();
                }
            })
            break
        case 3: 
            console.clear();
            rl.question(`Which One You Want To Calculate ?
            1. Area Of Cube
            2. Volume Of Cube
            Answer: `, answer => {
                if(answer == 1) {
                    rl.question('Minta Sisi Berapa ? ', s => {
                        console.log('Ya Sudah Ini Hasilnya: ',cube.cubeArea(s))
                        rl.close();
                    })
                } else if(answer == 2) {
                    rl.question('Minta Sisi berapa ?', s => {
                        console.log('Ya Sudah Ini Hasilnya: ',cube.cubeVolume(s))
                        rl.close();
                    })
                } else {
                    console.log(`Kok Sek Ndablek ${answer} iku Tidak Ada`)
                    rl.close();
                }
            })
            break
        case 4: 
            console.clear();
            rl.question('Minta Alas berapa ? ', b => {
                rl.question('Minta Tinggi Berapa?', h => {
                console.log('Ya Sudah Ini Hasilnya: ', triangle.trianglearea(+b, +h))
                register(triangle.trianglearea(+b, +h))
                rl.close();
            })
                })
            break
        default:
            console.log(`Tidak Ada Pilihan ${answer} Kok Sek Ngeyel, Blok`)
            rl.close();
        }
  });
  
  function register(result) {
    fs.writeFileSync(
        path.resolve(__dirname, '.', "data.json"), JSON.stringify({
            result: result,
            }, null, 2)
        )
}