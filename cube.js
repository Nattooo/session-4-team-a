function cubeArea(s) {
    if (typeof +s !== 'number') {
        throw new Error('Input is not valid')
    } else {
        const hasil = 12 * s;
        return hasil;
    }
};

function cubeVolume(s) {
    if (typeof +s !== 'number') {
        throw new Error('Input is not valid')
    } else {
        const hasil = s ** 3;
        return hasil;
    }
};

